<?php
require_once './vendor/box/spout/src/Spout/Autoloader/autoload.php';
require_once './libs/inflector_helper.php';
require_once './libs/Parser.php';

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

$reader = ReaderFactory::create(Type::XLSX); // for XLSX files
//$reader = ReaderFactory::create(Type::CSV); // for CSV files
//$reader = ReaderFactory::create(Type::ODS); // for ODS files

if(count($argv)<2){
	echo "Usage:php  run.php path/to/xlsx";
	exit;
}

$reader->open($argv[1]);

$ignore_sheet=['MASTER','TEMPLATE','List'];

$db_prefix=['dtb','mtb'];

$parser=new Parser();
$app_path="app/";
@mkdir($app_path);
$app_http_path=$app_path ."Http/";
@mkdir($app_http_path);
$controller_path=$app_http_path ."Controllers/";
@mkdir($controller_path);
$repository_path=$app_http_path ."Repositories/";
@mkdir($repository_path);

		

foreach ($reader->getSheetIterator() as $sheet) {
    $sheet_name = $sheet->getName();

    $final_name= singular(ucfirst(camelize($sheet->getName())));
    if(!in_array($final_name,$ignore_sheet)){
        echo "Generating models .... ".$final_name;
        echo "\r\n";
        $model_data=['ClassName'=>$final_name,'TableName'=>$sheet_name];
        $model_template=file_get_contents('templates/model.tpl');
        $model_content=$parser->parse_string($model_template,$model_data,TRUE);
        
		write_file($app_path.$final_name.'.php',$model_content);
		
		$controller=$final_name;
		foreach($db_prefix as $prefix){
			//$cname=strtolower($controller);
			if(preg_match("/".ucfirst($prefix)."/",$controller,$match)){
				$controller=str_replace(ucfirst($prefix),'',$controller);
			}
		}
		echo "Generating Controllers ...." . $controller ."Controller\r\n";
		$controller_data=['ModelName'=>$final_name,'Name'=>$controller];
        $controller_template=file_get_contents('templates/controller.tpl');
        $controller_content=$parser->parse_string($controller_template,$controller_data,TRUE);
        
		write_file($controller_path.$controller.'Controller.php',$controller_content);
    }
    
}

$reader->close();

function write_file($path, $data, $mode = 'w+')
	{
		if ( ! $fp = @fopen($path, $mode))
		{
			return FALSE;
		}

		flock($fp, LOCK_EX);
		fwrite($fp, $data);
		flock($fp, LOCK_UN);
		fclose($fp);

		return TRUE;
	}